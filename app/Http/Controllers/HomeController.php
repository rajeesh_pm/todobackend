<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Str;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            $user               =   Auth::user();
            $user->api_token    =   Str::random(60);
            $user->save();
            return $user;
        }
        return response()->json(['success' => false, 'message' => 'Invalid credentials'],422);
    }

    public function logout()
    {
        $user   =   Auth::user();
        if($user){
            $user->api_token    =   null;
            $user->save();
            return response()->json(['success' => true, 'message' => 'Logged out.']);
        }
        else{
            return response()->json(['success' => false, 'message' => 'Unable to logout the user.']);
        }
    }
}
