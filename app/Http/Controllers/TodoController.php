<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Todo;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $todo       =   new Todo();
        $userId     =   $request->userId;
        $exists     =   $todo->where('user_id',$userId)->exists();
        if($exists){
            $data       =   $todo->where('user_id',$userId)->latest('date')->get();
        }else{
            $data       =   [];
        }
        return response()->json(['success' => true, 'data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $todo           =   new Todo();
        $todo->todo     =   $request->content;
        $todo->user_id  =   $request->userId;
        $todo->date     =   $request->date;
        $todo->save();

        return response()->json(['success' => true, 'message' => 'todo saved']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $todo           =   new Todo();
        $todoId         =   $request->todoid;

        $exists         =   $todo->where('id',$todoId)->exists();
        if($exists){
            $update     =   $todo->where('id',$todoId)->update(['todo' => $request->todo]);
            return response()->json(['success' => true, 'message' => 'Updated']);
        }
        return response()->json(['success' => false, 'message' => 'Failed to update']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $todo           =   new Todo();
        $todoId         =   $request->todoid;

        $exists         =   $todo->where('id',$todoId)->exists();
        if($exists){
            $delete     =   $todo->where('id',$todoId)->delete();
            return response()->json(['success' => true, 'message' => 'Deleted']);
        }
        return response()->json(['success' => false, 'message' => 'Failed to delete']);
    }
}
