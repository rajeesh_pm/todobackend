<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', 'HomeController@login');
Route::get('/logout', 'HomeController@logout');
Route::middleware(['auth:api'])->group(function () {
    Route::post('/todostore', 'TodoController@store');
    Route::post('/todolist', 'TodoController@index');
    Route::post('/tododelete', 'TodoController@destroy');
    Route::post('/todoupdate', 'TodoController@update');
});